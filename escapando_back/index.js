/*
	react node
	escapando test
*/

// requires & vars
	const express    = require('express')
	const cors       = require('cors')
	const bodyParser = require('body-parser')

	const app        = express()

// app
	const Connection     = require('./db/connection')
	app.use(cors())
	// app.use(bodyParser.json())
	app.use(bodyParser.urlencoded({extended:true}));

	// route
		app.get('/', (req, res) => {
			res.send('route working')

			res.end()
		})

		// route user
			app.get('/user', (req, res) => {
				const get_users_query = 'SELECT * FROM user ORDER BY id'
				console.log(get_users_query)
				Connection.query(get_users_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.send(err)
					}else{
						console.log('ok')
						return res.json({
							data : results, 
							msg  : 'new user added!'
						})
					}
				})
			})

			// insert by post
			app.post('/user', (req, res) => {
				const {name, lastname, email, description, pass} = req.body
				const add_user_query  = `INSERT INTO user (name, lastname, email, description, pass) VALUES('${name}', '${lastname}', '${email}','${description}', '${pass}')`
				console.log(add_user_query)
				Connection.query(add_user_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.json({
							data : err, 
							msg  : 'db error'
						})
					}else{
						console.log('ok')
						return res.json({
							data : results,
							msg  : 'new user added!'
						})
					}
				})
			})

			// insert by get
			app.get('/user/new', (req, res) => {
				const {name, lastname, email, description, pass} = req.query
				const add_user_query  = `INSERT INTO user (name, lastname, email, description, pass) VALUES('${name}', '${lastname}', '${email}','${description}', '${pass}')`
				console.log(add_user_query)
				Connection.query(add_user_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.json({
							data : err, 
							msg  : 'db error'
						})
					}else{
						console.log('ok')
						return res.json({
							data : results,
							msg  : 'new user added!'
						})
					}
				})
			})

			// approve user
			app.post('/user/approve', (req, res) => {
				const {approve_id} = req.query
				const status = 'success'
				const update_user_query  = `UPDATE user SET status='${status}' WHERE id=${approve_id}`
				console.log(update_user_query)
				Connection.query(update_user_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.json({
							data : err, 
							msg  : 'db error'
						})
					}else{
						console.log('ok')
						return res.json({
							data : results,
							msg  : 'user approved!'
						})
					}
				})
			})

			// refuse user
			app.post('/user/refuse', (req, res) => {
				const {refuse_id} = req.query
				const status = 'warning'
				const update_user_query  = `UPDATE user SET status='${status}' WHERE id=${refuse_id}`
				console.log(update_user_query)
				Connection.query(update_user_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.json({
							data : err, 
							msg  : 'db error'
						})
					}else{
						console.log('ok')
						return res.json({
							data : results,
							msg  : 'user rejected!'
						})
					}
				})
			})

			// delete
			app.post('/user/delete', (req, res) => {
				const {delete_id} = req.query
				const delete_user_query  = `DELETE FROM user WHERE  id = ${delete_id}`
				console.log(delete_user_query)
				Connection.query(delete_user_query, (err, results) => {
					if (err) {
						console.log('error')
						return res.json({
							data : err, 
							msg  : 'db error'
						})
					}else{
						console.log('ok')
						return res.json({
							data : results,
							msg  : 'user deleted!'
						})
					}
				})
			})

	app.listen(8080, () => {
		console.log('server ok')
	})