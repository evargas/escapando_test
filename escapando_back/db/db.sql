-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para escapando_test_bd
DROP DATABASE IF EXISTS `escapando_test_bd`;
CREATE DATABASE IF NOT EXISTS `escapando_test_bd` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `escapando_test_bd`;

-- Volcando estructura para tabla escapando_test_bd.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `description` text COLLATE utf8_spanish_ci,
  `status` varchar(20) COLLATE utf8_spanish_ci DEFAULT 'secondary',
  `pass` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla escapando_test_bd.user: ~0 rows (aproximadamente)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `lastname`, `email`, `description`, `status`, `pass`, `created`, `updated`) VALUES
	(1, 'Erick', 'Vargas', 'erickorso@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo5897985 udg jc wc ckjwgc jhc ckqwech wkec facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iure e', 'warning', '1234', '2018-09-17 15:56:20', '2018-09-17 19:21:35'),
	(3, 'Rodrigo', 'Vargas', 'rodrigo@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo5897985 udg jc wc ckjwgc jhc ckqwech wkec facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iure e', 'warning', '1234', '2018-09-17 15:56:48', '2018-09-17 19:21:35'),
	(5, 'Sofia', 'Vargas', 'sofia@gmail.com', 'Lorem ipsum dolor sit amet, consect ckqwech wkec facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iure e', 'success', '12343', '2018-09-17 15:57:43', '2018-09-17 15:59:37'),
	(6, 'Oriana', 'Vargas', 'oriana@gmail.com', 'Lorem ipsum dolor sit amet, consect cde? Et nobis, placeat odio non corrupti molestiae iure e', 'success', '123431', '2018-09-17 15:58:35', '2018-09-17 16:13:20'),
	(7, 'Thaydee', 'de Vargas', 'thaydee@gmail.com', 'Lorem ipsum dolor sit amet, consect cde? Et nobis, placeat odio nojdslkhsd sd ksjdh sdk sjdh sdkj sdkj dn corrupti 7985 udg jc wc ckjwgc jhc ckqwech wkec facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iuremolestiae iure e', 'warning', '123431', '2018-09-17 15:59:05', '2018-09-17 19:21:35'),
	(8, 'test', 'tesedddd', 'emailDefault@gg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iure e', 'warning', '67890', '2018-09-17 16:12:48', '2018-09-17 19:19:19'),
	(9, 'new', 'undefined', 'email', 'este es un nuevo registro', 'secondary', '1234', '2018-09-17 19:23:24', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
