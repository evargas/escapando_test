import React, { Component } from 'react';
import logo from './logo.svg'; 
import defaultImage from './public/img/square.jpg';
import './public/css/materialize.min.css';
import './public/css/bootstrap.min.css';
import './public/css/font-awesome/css/font-awesome.min.css';

import './App.css';

class App extends Component {

  state = {
    users : [], 
    user  : {
      name        : 'Erick', 
      lastname    : 'Vargas', 
      email       : 'erickorso@gmail.com', 
      description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo facilis necessitatibus omnis unde? Et nobis, placeat odio non corrupti molestiae iure e',
      status      : 'secondary', 
      pass        : '1234'

    },
    usersSelected : [], 
    error : 'No Errors'
  }

  componentDidMount(){
    this.getUsers()
  }

  getUsers = _ => {
    fetch('http://localhost:8080/user')
      .then( res => res.json())
      .then( res => this.setState({users: res.data}))
      .catch( err => console.log(err))
  }

  addUser = _ => {
    const { user } = this.state
    fetch(`http://localhost:8080/user/new?name=${user.name}&lastname=${user.lastname}&email=${user.email}&description=${user.description}&pass=${user.pass}`)
      .then( this.setState({ error : 'Registro Exitoso'}))
      .catch( err => {
          this.setState({ error : 'Error Registro'})
          console.log(err)
      })
      .then( this.getUsers)
  }

  addUserPost = _ => {
    const { user } = this.state
    fetch(`http://localhost:8080/user?name=${user.name}&lastname=${user.lastname}&email=${user.email}&description=${user.description}&pass=${user.pass}`, {method: 'POST'})
      .then( res => {console.log(res)})
      .then( this.setState({ error : 'Registro Exitoso'}))
      .catch( err => {
          this.setState({ error : 'Error Registro'})
          console.log(err)
      })
      .then( this.getUsers)
  }

  deleteUser = e => {
    const deleteUserId = e.target.dataset.id
    console.log(e.target.dataset.id)
    if (window.confirm('Estas a punto de borrar el registro '+ deleteUserId)) {
      fetch(`http://localhost:8080/user/delete?delete_id=${deleteUserId}`, {method: 'POST'})
        .then( this.setState({ error : 'Registro Eliminado'}))
        .catch( err => {
            this.setState({ error : err.sqlMessage})
            console.log(err)
        })
        .then( this.getUsers)
    }
  }
  selectUser = e => {
    const selectUserId = e.target.dataset.id
    const { usersSelected } = this.state
    const position = usersSelected.indexOf(selectUserId)
    if (position === -1) {
      usersSelected.push(selectUserId)
    }else{
      usersSelected.splice(position, 1)
    }
    this.setState({usersSelected : usersSelected})
  }

  approveUser = e => {
    const { usersSelected } = this.state
    const approveUsers = usersSelected.length
    if (approveUsers > 0) {
      console.log(approveUsers);
      for (var i = 0; i < approveUsers; i++) {
        fetch(`http://localhost:8080/user/approve?approve_id=${usersSelected[i]}`, {method: 'POST'})
          .then( this.setState({ error : 'Registro Aprobados'}))
          .catch( err => {
              this.setState({ error : err.sqlMessage})
              console.log(err)
          })
          .then( this.getUsers)
      }
      this.setState({ 
          error         : 'Usuarios aprobados', 
          usersSelected : []
      })
    }else{
      this.setState({ error : 'No haz selecionado ningun usuario para aprobar'})
    }
  }

  refuseUser = e => {
    const { usersSelected } = this.state
    const refuseUsers = usersSelected.length
    if (refuseUsers > 0) {
      console.log(refuseUsers);
      for (var i = 0; i < refuseUsers; i++) {
        fetch(`http://localhost:8080/user/refuse?refuse_id=${usersSelected[i]}`, {method: 'POST'})
          .then( this.setState({ error : 'Registro Aprobados'}))
          .catch( err => {
              this.setState({ error : err.sqlMessage})
              console.log(err)
          })
          .then( this.getUsers)
      }
      this.setState({ 
          error         : 'Usuarios aprobados', 
          usersSelected : []
      })
    }else{
      this.setState({ error : 'No haz selecionado ningun usuario para aprobar'})
    }
  }

  renderUser = ({id, name, lastname, email, created}) =>  (
      <li key={id}>
          {name} {lastname} {email} {created} &nbsp;
          <button className="btn btn-large btn-outline-danger" key={id} data-id={id} onClick={this.deleteUser}>
              <i className="fa fa-close"></i>
          </button>
      </li>
      );

  renderUserCard = ({id, name, lastname, email, description, status, created}) =>  (
      <div className={'card bg-' + status} key={id}>
          <img className="card-img-top" src={ defaultImage } alt={name}/>
          <div className="card-body">
            <h5 className="card-title">{name} {lastname}</h5>
            <p className="card-text">{email} {created}</p>
            <p className="card-text text-justify">{description}</p>
            <button className="btn btn-small btn-outline-primary" data-id={id} onClick={this.selectUser}>
                <i className="fa fa-star"></i>
            </button>
            <button className="btn btn-small btn-outline-danger" data-id={id} onClick={this.deleteUser}>
                <i className="fa fa-close"></i>
            </button>
          </div>
      </div>
      );

  render() {
    const { users, user, error} = this.state; 
    return (
      <div className="App">
        <nav>
          <div className="nav-wrapper">
            <a href="./" className="brand-logo">
              <img src={logo} className="App-logo" alt="logo" />
            </a>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li><a href="./">Demo</a></li>
            </ul>
          </div>
        </nav>
        <h2>Seleccion de Participaciones</h2>
        <div className="wrapper">
          <div className="user-list">
            <h3>Mostrando {users.lenght} Participaciones</h3>
            <div className="card-columns">
              { users.map(this.renderUserCard) }
            </div>
          </div>
          <div className="user-actions">
            <div className="card border-info mb-3">
              <div className="card-header">Seleccion:</div>
              <div className="card-body text-primary">
                <h5 className="card-title">xx Participaciones</h5>
                <button className="btn btn-large btn-block btn-outline-success" onClick={this.approveUser}>
                  Aprobar <i className="fa fa-check"></i>
                </button> 
                <button className="btn btn-large btn-block btn-outline-warning" onClick={this.refuseUser}>
                  Rechazar <i className="fa fa-close"></i>
                </button>
              </div>
            </div>
            <div className="alert alert-primary" role="alert">
              {error}
            </div>
            <div className="card border-info mb-3">
              <div className="card-header">Add User:</div>
              <div className="card-body text-primary">
                <legend>Add User</legend>            

                <input className="form-control"
                  value={user.name}
                  onChange={e => this.setState({user : {...user, name:     e.target.value}})}/>
                <input className="form-control"
                  value={user.lastname}
                  onChange={e => this.setState({user : {...user, lastname: e.target.value}})}/>
                <input className="form-control"
                  value={user.email}
                  onChange={e => this.setState({user : {...user, email:    e.target.value}})}/>
                <input className="form-control"
                  value={user.pass}
                  onChange={e => this.setState({user : {...user, pass:     e.target.value}})}/>
                <textarea className="form-control"
                  value={user.description}
                  onChange={e => this.setState({user : {...user, description: e.target.value}})}></textarea>
                <button className="btn btn-large btn-block btn-info" onClick={this.addUser}>
                    New User GET &nbsp;
                    <i className="fa fa-save"></i>
                </button>
                <button className="btn btn-large btn-block btn-success" onClick={this.addUser}>
                    New User POST &nbsp;
                    <i className="fa fa-save"></i>
                </button>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
